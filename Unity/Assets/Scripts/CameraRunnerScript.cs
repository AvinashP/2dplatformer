﻿using UnityEngine;
using System.Collections;

public class CameraRunnerScript : MonoBehaviour {
	public Transform player;
	private Transform cameraTransform;

	void Awake() {
		cameraTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(null != player)
			cameraTransform.position = new Vector3(player.transform.position.x + 6 , player.transform.position.y + 1.8f, -10);
	}
}
