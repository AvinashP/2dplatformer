using UnityEngine;

[RequireComponent(typeof(GUIElement))]
public class Button : MonoBehaviour
{
    public string buttonName = "Fire1";                     // The name used for the button
    public bool pairedWithInputManager;
	public bool keepPressed = false;
	public bool fireOnce = false;

    private AbstractButton m_Button;

    void OnEnable()
    {
        m_Button = ButtonFactory.GetPlatformSpecificButtonImplementation ();
        m_Button.Enable(buttonName, pairedWithInputManager, GetComponent<GUIElement>().GetScreenRect(),keepPressed,fireOnce);
		GetComponent<GUITexture>().enabled = true;
    }


    void OnDisable()
    {
        // remove the button from the cross platform input manager
        m_Button.Disable ();
		GetComponent<GUITexture>().enabled = false;
    }

    void Update()
    {
        m_Button.Update ();
    }
}
