﻿using UnityEngine;

public abstract class AbstractButton {

    protected CrossPlatformInput.VirtualButton m_Button;        // A reference to the button in the cross platform input system
    protected Rect m_Rect;                                      // The zone on screen where the button exists
	protected bool m_keepPressed;
	protected bool m_fireOnce;

    public void Enable (string name,bool pairwithinputmanager, Rect rect, bool keepPressed_, bool fireOnce_) {

        m_Button = new CrossPlatformInput.VirtualButton(name, pairwithinputmanager);
        m_Rect = rect;
		m_keepPressed = keepPressed_;
		m_fireOnce = fireOnce_;
    }

    public void Disable () {

        m_Button.Remove();
    }

    // override this to implement the logic for new button types i.e. touch for mobile and click for windows/osx/linux
    public abstract void Update();

}
