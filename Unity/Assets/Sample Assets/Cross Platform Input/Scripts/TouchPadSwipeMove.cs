﻿using UnityEngine;
using System.Collections;

public class TouchPadSwipeMove : TouchPad {
	
	//public float m_margin = 0.1f;
	public Vector2 m_margin = new Vector2 (10.0f,10.0f);

	bool m_takeInput = false;

	Vector2 lastPosNext1 = Vector2.zero;
	Vector2 lastPosNext2 = Vector2.zero;

	protected override void ForEachTouch(Touch touch, Vector2 guiTouchPos)
	{
		base.ForEachTouch(touch, guiTouchPos);
		
		if (lastFingerId == touch.fingerId && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
		{
			m_takeInput = false;
			ResetJoystick();
		}
		
		if (touch.phase == TouchPhase.Began && touchZoneRect.Contains(touch.position))
		{
			m_takeInput = true;
		}
		
		if (lastFingerId != touch.fingerId || !m_takeInput)
			return;

		// position of touch relative to touch start position defines the input amount:
		if (touch.phase == TouchPhase.Began)
		{
			lastTouchPos = touch.position;
			lastPosNext2 = touch.position;
		}
		Vector2 newRelativeTouchPos = new Vector2((lastTouchPos.x - touch.position.x) / sensitivityRelativeX,  (lastTouchPos.y - touch.position.y) / sensitivityRelativeY);
		
		//Vector2 newPosition = Vector2.Lerp(position, newRelativeTouchPos * sensitivity * 2, Time.deltaTime * interpolateTime);
		
		float x = Mathf.Lerp(position.x, newRelativeTouchPos.x * sensitivity.x * 2, Time.deltaTime * interpolateTime.x);
		float y = Mathf.Lerp(position.y, newRelativeTouchPos.y * sensitivity.y * 2, Time.deltaTime * interpolateTime.y);
		
		Vector2 newPosition = new Vector3 (x, y);

		if (touch.deltaTime > 0)
		{
			if (useX)
			{
				float newx = newPosition.x * sensitivity.x;
				position.x = newx;
			}
			if (useY)
			{
				float newy = newPosition.y * sensitivity.y;
				position.y = newy;
			}
		}

	}
}
